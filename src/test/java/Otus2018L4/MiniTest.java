// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-02-06 17:50:42 korskov>

package Otus2018L4;

/**
 * Написать свой тестовый фреймворк.  
 * Поддержать аннотации @Test, @Before, @After.  
 * Запускать вызовом статического метода с:
 * 1. именем класса с тестами,
 * 2. именем package в котором надо найти и запустить тесты 
 */

import java.lang.Class;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
*/

/**
 * Unit test for simple App.
 */
public class MiniTest {
    static final Logger logger = LoggerFactory.getLogger (MiniTest.class);

    private Class<?> testClass;

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public MiniTest (Class testName) {
        super();
    }

    public MiniTest (String testClassName) {
        super();
        testClass = Class.forName (testClassName);
    }

    /**
     * @return the suite of tests being tested
     * Otus2018L4/MiniTest.java:[43,38] incompatible types: java.lang.Class<Otus2018L4.MiniTest> cannot be converted to java.lang.String
     */
    public static MiniTest suite (final String[] args) {
        return new MiniTest (args[0]);
    }

    /**
     * exec all "@Before"
     * exec all "@Test"
     * exec all "@After"
     */
    public void exec() {
        for (Method c2tm : testClass.getMethods()) {
            for (Annotation c2ta : testClass.getAnnotations()) {
                ;
            }
        }
    }
}
