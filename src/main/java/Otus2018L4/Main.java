// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-04-24 19:36:16 korskov>

package Otus2018L4;

/**
 * Написать свой тестовый фреймворк.  
 * Поддержать аннотации @Test, @Before, @After.  
 * Запускать вызовом статического метода с:
 * 1. именем класса с тестами,
 * 2. именем package в котором надо найти и запустить тесты 
 */

/*
import java.lang.annotation.Annotation;
import java.lang.annotation.Native;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
*/

import OuCeShi.OuCeShi;
import OuCeShi.Before;
import OuCeShi.Test;
import OuCeShi.After;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    static final Logger logger = LoggerFactory.getLogger (Main.class);

    public static void main (String[] args) {
        logger.info("OuCeShi demo");
        OuCeShi.EvalTestClass(Main.class);
    }

    Main() {
        logger.info ("called: constructor Main");
    }

    @Before
    public void doBefore() {
        logger.info ("called: doBefore");
        System.out.println("prepare suite");
    }

    @After
    public void doAfter() {
        logger.info ("called: doAfter");
        System.out.println("tear down suite");
    }

    @Test
    public void doTest01() {
        logger.info ("called: test 01");
        System.out.println("test case 01");
    }
}
