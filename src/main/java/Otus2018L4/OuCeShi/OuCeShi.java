// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-04-24 19:39:19 korskov>

package OuCeShi;

/**
 * Написать свой тестовый фреймворк.  
 * Поддержать аннотации @Test, @Before, @After.  
 * Запускать вызовом статического метода с:
 * 1. именем класса с тестами,
 * 2. именем package в котором надо найти и запустить тесты 
 */

import java.util.ArrayList;
import java.lang.annotation.Annotation;
import java.lang.annotation.Native;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OuCeShi {
    static final Logger logger = LoggerFactory.getLogger(OuCeShi.class);

    public static void main (final String[] args) {
        for (String arg : args)
            System.out.println (arg);
    }

    /**
     * Запускать вызовом статического метода с:
     * 1. именем класса с тестами,
     */
    public static boolean EvalTestClass(final Class<?> testClass) {
        logger.info ("called: EvalTestClass");
        boolean testResult = true;
        OuCeShi ceShi = new OuCeShi();
        int n = ceShi.read(testClass);
        logger.info ("EvalTestClass: test to eval: " + n);
        if (n <= 0)
            return testResult;
        ceShi.prepare();
        ceShi.evalBefore();
        ceShi.eval();
        ceShi.evalAfter();
        return testResult;
    }

    /**
     * Запускать вызовом статического метода с:
     * 2. именем package в котором надо найти и запустить тесты 
     */
    public static boolean EvalTestPackage() {
        return false;
    }

    private Class<?> suite;
    private Object suiteEntity;
    private ArrayList<Method> before, tests, after;
    private OuCeShi() {
        suite = null;
        suiteEntity = null;
        before= new ArrayList<Method>();
        tests = new ArrayList<Method>();
        after = new ArrayList<Method>();
    }

    /**
     * сборка списков исполнения
     */
    protected int read(final Class suite) {
        logger.info ("called: read");
        this.suite = suite;
        for(Method met : suite.getDeclaredMethods()) {
            for(Annotation anno : met.getDeclaredAnnotations()) {
                if (anno instanceof Before) {
                    before.add(met);
                    break;
                }
                if (anno instanceof Test) {
                    tests.add(met);
                    break;
                }
                if (anno instanceof After) {
                    after.add(met);
                    break;
                }
            }
        }
        return before.size() + tests.size() + after.size();
    }

    /**
     * подготовка исполнения
     * загрузка класса, инстанцирование, ...
     */
    protected boolean prepare() {
        logger.info ("called: prepare");
        // instance of...
        try {
            this.suiteEntity = this.suite.newInstance();
        }
        catch (Exception ex) {
            // InstantiationException
            // IllegalAccessException
            // ...
            this.suiteEntity = null;
        }
        return this.suiteEntity != null;
    }

    /**
     * исполнительная обёртка:
     * prepare list
     */
    protected boolean evalBefore() {
        return evalList(this.before);
    }

    /**
     * исполнительная обёртка:
     * фактически, тесты
     */
    protected boolean eval() {
        return evalList(this.tests);
    }

    /**
     * исполнительная обёртка:
     * tear down list
     */
    protected boolean evalAfter() {
        return evalList(this.after);
    }

    /**
     * фактический исполнитель всего
     */
    private boolean evalList(final ArrayList <Method> evalL) {
        logger.info ("called: evalList");
        boolean res = true;
        for (Method evalIt : evalL)
            try {
                logger.info ("evalList: eval: " + evalIt.getName());
                evalIt.invoke(this.suiteEntity);
            }
            catch (Exception ex) {
                res = false;
            }
        return res;
    }
}
