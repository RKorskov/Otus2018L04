-*- mode: markdown; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
-*- eval: (set-language-environment Russian) -*-  
--- Time-stamp: <2019-04-24 14:15:35 korskov>

Overview
========

otus-java_2018-10

Группа 2018-10  
Студент:  
[Roman Korskov (Роман Корсков)](https://gitlab.com/RKorskov/otus-java_2018-10)  
korskov.r.v@gmail.com

[Разработчик Java  
Курс об особенностях языка и платформы Java, стандартной библиотеке, о
проектировании и тестировании, о том, как работать с базами, файлами,
веб-фронтендом и другими
приложениями](https://otus.ru/lessons/razrabotchik-java/)

# L4 Тестовый фреймворк на аннотациях

Написать свой тестовый фреймворк.  
Поддержать аннотации @Test, @Before, @After.  
Запускать вызовом статического метода с:
1. именем класса с тестами;
2. именем package в котором надо найти и запустить тесты 
